<?php
/**
 *  This page will dynamically ask for the required config that are missing
 */

$db_config = [
  'DB_HOST' => 'localhost',
  'DB_USER' => 'root',
  'DB_PASS' => 'password',
  'DB_NAME' => 'playtv',
  'SETTING_TABLE_NAME' => 'tbl_setting',
  'VIDEO_TABLE_NAME' => 'tbl_video'
];
$other_config = [
  'title_api' => 'API details',
  'APP_KEY' => 'App token',
  'APP_ID' => 'App ID',
  'title_personal' => 'Notification Setting',
  'NAME' => 'Full Name',
  'EMAIL' => 'Email Address'
];


$needed_config = file_exists('src/db_config.php') ? $other_config : $db_config;

echo Utils::generateForm($_ENV, $needed_config);