<?php

class Video {

  public $row_id;
  public $title;
  public $url;
  public $thumb_url;
  public $game_id;
  public $author_id;
  public $video_id;
  public $uploaded_at;

  private function __construct($raw_data) {
    if (property_exists($raw_data, 'video_id')) {
      $this->video_id = $raw_data->video_id;
    }
    if (property_exists($raw_data, 'id')) {
      $this->row_id = $raw_data->id;
    }
    if (property_exists($raw_data, 'thumbnail')) {
      $this->thumb_url = $raw_data->thumbnail;
    }
    if (property_exists($raw_data, 'upload_time')) {
      date_default_timezone_set("Asia/Bangkok");
      $this->uploaded_at = date('Y-m-d', $raw_data->upload_time);
    }
    if (property_exists($raw_data, 'game')) {
      $this->game_id = $raw_data->game->id;
    }
    if (property_exists($raw_data, 'author')) {
      $this->author_id = $raw_data->author->id;
    }
    if (property_exists($raw_data, 'link')) {
      $this->url = $raw_data->link;
    }
    if (property_exists($raw_data, 'description')) {
      $this->title = $raw_data->description;
    }
  }

  public static function save($raw_data) {
    return VideoTable::insert(new Video($raw_data));
  }

}