<?php

class Utils
{
    
    public static function generateForm($env = array(), $needed_config = array())
    {
        $html = '';
        $input_template = '<label>%s</label><input name="%s" placeholder="%s" type="text" required />';
        foreach ($needed_config as $config => $placeholder) {
            if (substr($config, 0, 5) == 'title') {
                $html .= "<h3>$placeholder</h3>";
            } elseif (!array_key_exists($config, $env)) {
                if (stristr($config, '_pass')) {
                    $html .= sprintf(str_replace('required', '', $input_template), str_replace('_', ' ', $config), $config, $placeholder);
                } else {
                    $html .= sprintf($input_template, str_replace('_', ' ', $config), $config, $placeholder);
                }
            }
        }
        return '<form class="guide" action="" method="POST">'.$html.'<button type="submit" class="primary-button u-full-width">Save</button></form>';
    }

    public static function loadConfig()
    {
        $sql = 'SELECT * FROM '.SETTING_TABLE_NAME;
        $rows = Connection::fetch($sql);
        $temp = array();
        foreach ($rows as $row) {
            $temp[$row['name']] = $row['value'];
        }
        return $temp;
    }

    public static function findEmptyKey($arr = array())
    {
        $empty_fields = array();
        foreach ($arr as $key => $value) {
            //ignore db_pass
            if (stristr($key, '_pass')) {
                continue;
            }
            if (null === $value || trim($value) === "") {
                $empty_fields[] = str_replace('_', ' ', $key);
            }
        }
        return $empty_fields;
    }
    
    /**
     * Creates src/db_config.php file by reading the data from POST request
     *
     * @return void
     */
    public static function createConfig()
    {
        $define_syntax = "define('%s', '%s');";
        $config = array();
        foreach ($_POST as $key => $value) {
            $config[] = sprintf($define_syntax, $key, $value);
        }
        file_put_contents('src/db_config.php', "<?php\r\n".implode("\r\n", $config));
    }

    public static function saveSetting($data = null)
    {
        if ($data == null) {
            $data = $_POST;
        }
        foreach ($data as $key => $value) {
            SettingTable::put($key, $value);
        }
    }
}
